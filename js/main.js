var btn = document.querySelector('a.btn');

btn.onclick = function (e) {
   e.preventDefault();
   var text = document.querySelector('p.intro');
   text.classList.add('red');
   var img = document.querySelector('img.desktop');
   img.style.display ='none';
   document.querySelector('.expectation-travel').style.marginTop = "100px";
}

$(function() {
   $(window).scroll(function() {
      $('#future .section-title').each(function() {
         var imagePos = $(this).offset().top;
   
         var topOfWindow = $(window).scrollTop();
         if (imagePos < topOfWindow+650) {
            $(this).addClass("fadeInDown");
            
         }
      });
   }); 
   $(window).scroll(function() {
      $('.expectation').each(function() {
         var imagePos = $(this).offset().top;
   
         var topOfWindow = $(window).scrollTop();
         if (imagePos < topOfWindow+650) {
            $(this).addClass("fadeInLeft");
            
         }
      });
   }); 
   $(window).scroll(function() {
      $('#expectation-travel').each(function() {
         var imagePos = $(this).offset().top;
   
         var topOfWindow = $(window).scrollTop();
         if (imagePos < topOfWindow+650) {
            $(this).addClass("fadeInUp");
            
         }
      });
   });
   $(window).scroll(function() {
      $('#expectation-value').each(function() {
         var imagePos = $(this).offset().top;
   
         var topOfWindow = $(window).scrollTop();
         if (imagePos < topOfWindow+650) {
            $(this).addClass("fadeInRight");
            
         }
      });
   });
   $(window).scroll(function() {
      $('#form .section-title').each(function() {
         var imagePos = $(this).offset().top;
   
         var topOfWindow = $(window).scrollTop();
         if (imagePos < topOfWindow+650) {
            $(this).addClass("fadeInDown");
            
         }
      });
   });
   $(window).scroll(function() {
      $('#btn-bottom').each(function() {
         var imagePos = $(this).offset().top;
   
         var topOfWindow = $(window).scrollTop();
         if (imagePos < topOfWindow+650) {
            $(this).addClass("fadeInUpBig");
            
         }
      });
   });    
})


